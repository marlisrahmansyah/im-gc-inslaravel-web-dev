<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="v
	viewport" content="width=device-width, initial-scale=1.0">
    <title>Garuda Cyber Institute</title>
</head>
<body>
    <h1>Garuda Cyber Institute</h1>
    <h3>Jadilah Programmer Handal Bersama GC-INS</h3>
    <p>Grow Together With Garuda Cyber Institute</p>
    <h3>Syarat dan Ketentuan</h3>
    <ul>
        <li>Tamatan SMA/SMK</li>
        <li>Tamatan Perguruan Tinggi</li>
        <li>Pekerja IT</li>
        <li>Freelancer</li>
    </ul>
    <h3>Cara Bergabung</h3>
    <ol>
        <li>Kunjungi website GC-INS</li>
        <a href="{{ route('register') }}">Register</a>
        <li>Lakukan Pembayaran</li>
    </ol>
</body>
</html>